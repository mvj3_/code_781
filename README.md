    十分方便给任意UIView（或者任意继承UIView的对象）加上点击动作响应事件。包括手指按下（touch up），手指松开（touch down），点击（touch down then up），双击（double taps），以及两只手指点击（two-finger taps）动作。	
